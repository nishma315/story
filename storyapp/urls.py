from django.urls.conf import path
from django.urls.resolvers import URLPattern

from .views import *



urlpatterns = [
    path('story-create/',StoryCreateView.as_view(),name='storycreate'),
    path('stories/',StoryListView.as_view(), name='storylist'),
    path('story-<id>/update/',StoryUpdateView.as_view(), name='storyupdate'),
    path('story-<id>/delete/',StoryDeleteView.as_view(), name='storydelete'),
   
   
    # path('story-media/create/', StoryMediaCreateView.as_view(), name='storymediacreate'),
    path('story-medias/', StoryMediaListView.as_view(), name ='storymedialiat'),
    path('story-media/<id>/update/',StoryMediaUpdateView.as_view(), name='storymediaupdate'),
    path('story-media/<id>/delete/',StoryMediaDeleteView.as_view(), name='storymediadelete'),
]

