from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Story, Story_Media







class StoryMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Story_Media
        fields = ['id','story','file','isTitlePicture']


class StorySerializer(serializers.ModelSerializer):
    story_media = StoryMediaSerializer(read_only=True, many=True)
    class Meta:
        model = Story
        fields = ['id','title','description','story_media']



