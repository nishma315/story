from django.db import models

class Story(models.Model):
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(null=True,blank=True)
    date_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.id} Story'

class Story_Media(models.Model):
    story = models.ForeignKey(Story,on_delete=models.CASCADE, null=True, related_name = 'story_media', related_query_name = 'story_media')
    file = models.FileField(upload_to='story_media/',  null=True)
    isTitlePicture = models.BooleanField(blank=False, null=True)

    def __str__(self):
        return f'{self.id} Media'


        