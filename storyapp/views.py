from django.shortcuts import render
from rest_framework.views import APIView
from .serializers import StoryMediaSerializer, StorySerializer
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status, viewsets
from .models import Story, Story_Media




class StoryCreateView(APIView):
    parser_classes = (MultiPartParser, FormParser,)
    def post(self, request):
        story_serializer = StorySerializer(data=request.data)
        if story_serializer.is_valid():
            c=story_serializer.save()
            isTitlePicture = request.data.get("isTitlePicture")
            for file in self.request.FILES.getlist('file'):
                fi = StoryMediaSerializer(data={'story': c.id,"file":file,"isTitlePicture":isTitlePicture})
                if fi.is_valid():
                    fi.save()
                else:
                    return Response({'Message':'Failed'},status=status.HTTP_400_BAD_REQUEST)

            return Response({'Message':'Created Successfully'},status=status.HTTP_201_CREATED)
        else:
            return Response({'Message':'Failed'},status=status.HTTP_400_BAD_REQUEST)




class StoryListView(APIView):
    def get(self,request):
        story = Story.objects.all()
        serializer = StorySerializer(story,many=True)
        return Response({'Message':'success','data':serializer.data},status=status.HTTP_200_OK)


class StoryUpdateView(APIView):
   
    def get(self,request,id):
        try:
            story = Story.objects.get(id=id)
            
        except Exception as e:
            print(e)
            story = None
        serializer = StorySerializer(story, many=False)

        return Response({'Message':'success','data':serializer.data},status=status.HTTP_200_OK)
    def post(self, request,id):
        try:
            story = Story.objects.get(id=id)
            
        except Exception as e:
            print(e)
            story = None
        serializer = StorySerializer(story, data=request.data, partial=True)
        if serializer.is_valid():
            c=serializer.save()

            for img in request.data.get("story_media"):
                 
                if img.get("id"):
                    spec = Story_Media.objects.get(id=img.get("id"))
                    spec.file = img.get("file")
                    spec.isTitlePicture = img.get("isTitlePicture")
                    if spec.isTitlePicture ==True:
                       spec.save()
                       spec.story.story_media.exclude(id=spec.id).update(isTitlePicture=False)
                    else:
                        pass
                  
                else:
                    StoryMediaSerializer(data={'story': c.id,"file":img.get("file"),"isTitlePicture":img.get("isTitlePicture")})
                
            return Response({'Message':'Updated successfully','data':serializer.data}, status=status.HTTP_201_CREATED)
        else:
            return Response({'Message':'Failed'}, status=status.HTTP_400_BAD_REQUEST)


class StoryDeleteView(APIView):
    
    def delete(self, request, id, format=None):
        story = Story.objects.get(id=id)
        story.delete()
        return Response({'Message':'Deleted Successfully'},status=status.HTTP_204_NO_CONTENT)


# class StoryMediaCreateView(APIView):
#     parser_classes = (FormParser, MultiPartParser)
#     def post(self, request):
#         serializer = StoryMediaSerializer(data=request.data)
#         if serializer.is_valid():
#             story = request.data.get("story")
#             s=Story.objects.get(id=story)
#             isTitlePicture = request.data.get("isTitlePicture")
#             for file in self.request.FILES.getlist('file'):
#                 fi = Story_Media(story=s,file=file,isTitlePicture=isTitlePicture)
#                 fi.save()
#             return Response(status=status.HTTP_201_CREATED)
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class StoryMediaListView(APIView):
    def get(self,request):
        storym = Story_Media.objects.all()
        serializer = StoryMediaSerializer(storym,many=True)
        return Response({'Message':'success','data':serializer.data},status=status.HTTP_200_OK)


class StoryMediaUpdateView(APIView):
    def get(self,request,id):
        try:
            storym = Story_Media.objects.get(id=id)    
        except Exception as e:
            print(e)
            storym = None
        serializer = StoryMediaSerializer(storym, many=False)

        return Response({'Message':'success','data':serializer.data},status=status.HTTP_200_OK)
    def post(self, request,id):
        try:
            storym = Story_Media.objects.get(id=id)
            
        except Exception as e:
            print(e)
            storym = None
        serializer = StoryMediaSerializer(storym, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'Message':'Updated Successfully','data':serializer.data},status=status.HTTP_201_CREATED)
        else:
            return Response({'Message':'Failed'}, status=status.HTTP_400_BAD_REQUEST)

class StoryMediaDeleteView(APIView):
    def delete(self, request, id, format=None):
        story = Story_Media.objects.get(id=id)
        story.delete()
        return Response({'Message':'Deleted Successfully'},status=status.HTTP_204_NO_CONTENT)